<?php

namespace App\Http\Controllers\Site;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Contact;
use Request as RequestFacade;
use Mail;

class ContactController extends Controller
{
    public function index()
    {
        $data = [
            'title' => 'Contacts',
            'description' => 'Contact us'
        ];

        return view('site.contacts', $data);
    }

    public function send(Request $request)
    {

        $result = false;
        $valid = false;
        if (!empty($request->phone)) {
            $valid = $this->phonValidate($request->phone);
        }


        if(!empty($request->name) && !empty($request->message) && $valid)
        {
            Contact::create([
                'name'    => $request->name,
                'phone'   => $request->phone,
                'message' => $request->message,
            ]);
            $result = true;
        }
        
        return response()->json(['result' => $result, 'name'=>$request->name]);
    }

    private function phonValidate($phon)
    {
        // build API request
        $APIUrl = 'https://api.phone-validator.net/api/v2/verify';
        $PhoneNumber = $phon;
        $CountryCode = "ua";
        $Locale = "ua";
        $Params = ['PhoneNumber' => $PhoneNumber,
                   'CountryCode' => $CountryCode,
                   'Locale'      => $Locale,
                   'APIKey'      => env('API_VER_PHONE')];
        $Request = http_build_query($Params, '', '&');
        $ctxData = [
            'method'  => "POST",
            'header'  => "Connection: close\r\n" .
                "Content-Type: application/x-www-form-urlencoded\r\n" .
                "Content-Length: " . strlen($Request) . "\r\n",
            'content' => $Request];
        $ctx = stream_context_create(['http' => $ctxData]);

        // send API request
        $result = json_decode(file_get_contents(
            $APIUrl, false, $ctx));

        // check API result
        switch ($result->{'status'}) {
            case "VALID_CONFIRMED":
                $valid = true;
                break;
            case "VALID_UNCONFIRMED":
                $linetype = $result->{'linetype'};
                $location = $result->{'location'};
                $countrycode = $result->{'countrycode'};
                $formatnational = $result->{'formatnational'};
                $formatinternational = $result->{'formatinternational'};
                $mcc = $result->{'mcc'};
                $mnc = $result->{'mnc'};
                $valid = false;
                break;
            case "INVALID":
                $valid = false;
                break;
            default:
                $valid = false;
        }
        return $valid;
    }
}
