@extends('site.layouts._layout')

@section('content')

    <section id="content">
        <div class="container">
            <div class="row">
                <div class="col-lg-12">
                    <h4>Contact us</h4>
                    <form id="contactform" method="POST" class="validateform">
                        <div id="sendmessage">
                            Thank you, <span id="name"></span>, we will process your request soon
                        </div>
                        <div id="senderror">
                            An error occurred while sending the message. Please check phone number or send mail for administrator's <span>{{ env('MAIL_ADMIN_EMAIL') }}</span>
                        </div>
                        <div class="row">
                            <div class="col-lg-4 field">
                                <input type="text" name="name" placeholder="* Enter your name" required />
                            </div>
                            <div class="col-lg-4 field">
                                <input type="text" id="phone" name="phone" placeholder="* Enter your phone " required />
                            </div>
                            <div class="col-lg-12 margintop10 field">
                                <textarea rows="12" name="message" class="input-block-level" placeholder="* Enter your message..." required></textarea>
                                <p>
                                    <button class="btn btn-theme margintop10 pull-left" type="submit">Send</button>
                                    <span class="pull-right margintop20">* Please fill in all required fields!</span>
                                </p>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </section>
@stop